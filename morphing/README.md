# morphing
> 文档请看[a9ia博客](https://www.a9ia.top/cv-morphing)
> 这部分示例图和原理是来自xjtu苏有歧老师

+ 项目效果
  苏老师的效果：
  ![image-example](https://cdn.jsdelivr.net/gh/a9ia/image/blog/morphingEx.png)
  我的效果：
  ![image-ex](https://cdn.jsdelivr.net/gh/a9ia/image/blog/image-hechengList.png)

+ 项目目录
  + modifyPic.py: 修改图片为某尺寸
  + createPoint.py: 需要依次点击两张图片的对应点组，手动创造60组图片对应点，并保存在 p1.csv p2.csv
  + morphing.py: 实现 morphing 图像变换，通过后两个参数可以调整形状和颜色的变换比重
  + p1.*: 图片1相关数据
  + p2.*: 图片2相关数据
  + example*.jpg: 示例图片
  + res.jpg


+ 项目环境
  python 3.7

+ 涉及到的库文件
  这些文件需要通过 `pip install` 进行安装
  + PIL
  + numpy
  + cv2
  + scipy
  + matplotlib

+ 如何运行
  + 如果要自己处理图片
    + 将图片添加进当前文件夹
    + 修改modifyPic 替换图片地址和目的图片，并生成p1.jpg, p2.jpg
    + 运行createPoint，轮流点击两张图片生成60个点集
    + 运行morphing进行图像处理，最终生成