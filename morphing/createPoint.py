'''
Date: 2022-06-27 14:57:47
LastEditTime: 2022-06-27 15:24:18
'''
from PIL import Image
from pylab import *
import numpy as np
import matplotlib.pyplot as plt

im1 = array(Image.open('p1.jpg'))
im2 = array(Image.open('p2.jpg'))
figure()
subplot(2, 1, 1)
imshow(im1)
subplot(2, 1, 2)
imshow(im2)
# 获取点集坐标
print ('Please click 40 points on 2 pictures in sequence')
x =ginput(60, 400)
print ('you clicked:',x)
plt.close()
# 处理<依次>点击两张图片对应点生成的集合
p1 = np.array(x[:: 2]) # 图像 1 点集
p2 = np.array(x[1:: 2]) # 图像 2 点集

# 写入文件保存
np.savetxt(fname="p1.csv", X=p1, fmt="%f", delimiter=",")
np.savetxt(fname="p2.csv", X=p2, fmt="%f", delimiter=",")